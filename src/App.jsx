import { useState } from "react";
import "./App.css";

function App() {
  const [items, setItems] = useState([
    {
      name: "Svelte",
      isChecked: false,
      isLeft: true,
    },
    {
      name: "JS",
      isChecked: false,
      isLeft: true,
    },
    {
      name: "Html",
      isChecked: false,
      isLeft: true,
    },
    {
      name: "CSS",
      isChecked: false,
      isLeft: true,
    },

    {
      name: "TS",
      isChecked: false,
      isLeft: false,
    },
    {
      name: "React",
      isChecked: false,
      isLeft: false,
    },
    {
      name: "Angular",
      isChecked: false,
      isLeft: false,
    },
    {
      name: "Vue",
      isChecked: false,
      isLeft: false,
    },
  ]);

  function selected(e) {
    setItems(
      items.map((entry) => {
        if (entry.name == e.name) {
          entry.isChecked = !entry.isChecked;
        }
        return entry;
      })
    );
  }

  function hardLeft() {
    setItems(
      items.map((entry) => {
        entry.isLeft = true;
        return entry;
      })
    );
  }

  function hardRight() {
    setItems(
      items.map((entry) => {
        entry.isLeft = false;
        return entry;
      })
    );
  }

  function left() {
    setItems(
      items.map((entry) => {
        if (entry.isChecked && !entry.isLeft) {
          entry.isChecked = false;
          entry.isLeft = true;
        }
        return entry;
      })
    );

    document.querySelectorAll(".rightcheck").checked = false;
  }
  function right() {
    setItems(
      items.map((entry) => {
        if (entry.isChecked && entry.isLeft) {
          entry.isChecked = false;
          entry.isLeft = false;
        }
        return entry;
      })
    );
    document.querySelector(".leftcheck").checked = false;
  }

  return (
    <>
      <div className="container">
        <div className="left">
          {items
            .filter((entries) => {
              return entries.isLeft;
            })
            .map((entries) => {
              return (
                <div key = {entries.name}
                className="card">
                  <input
                    type="checkbox"
                    onClick={() => selected(entries)}
                    className="leftcheck"
                  />
                  <div>{entries.name}</div>
                </div>
              );
            })}
        </div>
        <div className="btnDiv">
          <button onClick={() => hardLeft()}>HardLeft</button>
          <button onClick={() => left()}>Left</button>
          <button onClick={() => right()}>Right</button>
          <button onClick={() => hardRight()}>HardRight</button>
        </div>
        <div className="right">
          {items
            .filter((entries) => {
              return !entries.isLeft;
            })
            .map((entries) => {
              return (
                <div key = {entries.name}
                 className="card">
                  <input
                    type="checkbox"
                    onClick={() => selected(entries)}
                    className="rightcheck"
                  />
                  <div>{entries.name}</div>
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
}

export default App;
